-- Run the MySQL/Maria DB in Terminal
mysql -u root -p

-- Show/retrieve all the databases:
SHOW DATABASES;

-- Create/add a database:
-- Syntax: CREATE DATABASE database_name;
CREATE DATABASE blog_db;

-- Select/Use a database;
-- Syntax: USE database_name
USE blog_db;

-- Create/Add a tables.
-- Syntax: CREATE TABLE table_name(column1, column2, PRIMARY KEY (id));
CREATE TABLE author(
au_id VARCHAR(50) NOT NULL,
au_lname VARCHAR(50) NOT NULL,
au_fname VARCHAR(50) NOT NULL,
address VARCHAR(50) NOT NULL,
city VARCHAR(50) NOT NULL,
state VARCHAR(50) NOT NULL,
PRIMARY KEY(au_id)
);

--Insert data to tables
--Syntax: INSERT INTO table_name(a,b,c) VALUES(1,2,3),(4,5,6),(7,8,9);
INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
	VALUES ('172-32-1176', 'White', 'Johnson', '10932 Bigge Rd.', 'Menlo Park', 'CA');

INSERT INTO author (au_id, au_lname, au_fname, address, city, state)
	VALUES ('213-46-8915', 'Green', 'Marjorie', '309 63rd St. #411', 'Oakland', 'CA'), ('238-95-7766', 'Carson', 'Cheryl', '589 Darwin Ln.', 'Berkeley', 'CA'),
	('267-41-2394', 'OLeary', 'Michael', '22 Cleveland Av. #14', 'San Jose', 'CA'), ('274-80-9391', 'Straight', 'Dean', '5420 College Av.', 'Oakland', 'CA'),
	('341-22-1782', 'Smith', 'Meander', '10 Missussuppi Dr.', 'Lawrence', 'KS'), ('409-56-7008', 'Bennet', 'Abraham', '6223 Bateman St.', 'Berkeley', 'CA'),
	('427-17-2319', 'Dull', 'Ann', '3410 Blonde St.', 'Palo Alto', 'CA'), ('472-27-2349', 'Gringlesby', 'Burt', 'P0 Box 792', 'Covelo', 'CA'),
	('486-29-1786', 'Looksley', 'Charlene', '18 Broadway Av.', 'San Francisco', 'CA');

-- Create/Add a tables.
-- Syntax: CREATE TABLE table_name(column1, column2, PRIMARY KEY (id));
CREATE TABLE publisher(
pub_id INT NOT NULL,
pub_name VARCHAR(50) NOT NULL,
city VARCHAR(50) NOT NULL,
PRIMARY KEY(pub_id)
);

--Insert data to tables
--Syntax: INSERT INTO table_name(a,b,c) VALUES(1,2,3),(4,5,6),(7,8,9);

INSERT INTO publisher (pub_id, pub_name, city)
	VALUES ('736', 'New Moon Books', 'Boston'), ('877', 'Binnet & Hardley', 'Washington'), ('1389', 'Algodata Infosystems', 'Berkeley'), ('1622', 'Five Lakes Publishing', 'Chicago'), ('1756', 'Ramona Publishers', 'Dallas'), ('9901', 'GGG&G', 'Munchen'), ('9952', 'Scootney Books', 'New York'), ('9999', 'Lucerne Publushing', 'Paris');

-- Create/Add a tables.
-- Syntax: CREATE TABLE table_name(column1, column2, PRIMARY KEY (id));
CREATE TABLE title(
title_id VARCHAR(50) NOT NULL,
title VARCHAR(50) NOT NULL,
type VARCHAR(50) NOT NULL,
price DECIMAL(8,2) NOT NULL,
pub_id INT NOT NULL,
PRIMARY KEY(title_id),
CONSTRAINT fk_publisher_pub_id
	FOREIGN KEY(pub_id) REFERENCES publisher(pub_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

--Insert data to tables
--Syntax: INSERT INTO table_name(a,b,c) VALUES(1,2,3),(4,5,6),(7,8,9);

INSERT INTO title (title_id, title, type, price, pub_id)
	VALUES ('BU1032', 'The Busy Executives Database Guide', 'business', 19.99, 1389);

INSERT INTO title (title_id, title, type, price, pub_id)
	VALUES ('BU1111', 'Cooking with Computers', 'business', 11.95, 1389), ('BU2075', 'You Can Combat Computer Stress!', 'business', 2.99, 736), ('BU7832', 'Straight Talk About Computers', 'business', 19.99, 1389), ('MC2222', 'Silicon Valley Gastronomic Treats', 'mod_cook', 19.99, 877), ('MC3021', 'The Gourmet Microwave', 'mod_cook', 2.99, 877), ('MC3026', 'The Psychology of Computer Cooking', 'UNDECIDED', '', 877), ('PC1035', 'But Is It User Friendly?', 'popular_comp', 22.95, 1389), ('PC8888', 'Secrets of Silicon Valley', 'popular_comp', 20, 1389), ('PC9999', 'Net Etiquette', 'popular_comp', '', 1389), ('PS2091', 'Is Anger the Enemy', 'psychology', 10.95, 736);

-- Create/Add a tables.
-- Syntax: CREATE TABLE table_name(column1, column2, PRIMARY KEY (id));
CREATE TABLE author_title(
au_id VARCHAR(50) NOT NULL
title_id VARCHAR(50) NOT NULL,
CONSTRAINT fk_author_au_id
	FOREIGN KEY(au_id) REFERENCES author(au_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT fk_title_title_id
	FOREIGN KEY(title_id) REFERENCES title(title_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);


PS: GOT AN ERROR IN INSERTING THE DATA SINCE THEY AREN'T PRESENT ON THE REFERENCE or FOREIGN KEY DATA, MOVE TO THE NEXT ONE: MariaDB [blog_db]> INSERT INTO author_title (au_id, title_id)
    ->  VALUES ('172-32-1176', 'PS3333');
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`blog_db`.`author_title`, CONSTRAINT `fk_title_title_id` FOREIGN KEY (`title_id`) REFERENCES `title` (`title_id`) ON UPDATE CASCADE)

--Insert data to tables
--Syntax: INSERT INTO table_name(a,b,c) VALUES(1,2,3),(4,5,6),(7,8,9);

INSERT INTO author_title (au_id, title_id)
	VALUES ('213-46-8915', 'BU1032');

INSERT INTO author_title (au_id, title_id)
	VALUES ('213-46-8915', 'BU2075');

INSERT INTO author_title (au_id, title_id)
	VALUES ('238-95-7766', 'PC1035');

INSERT INTO author_title (au_id, title_id)
	VALUES ('267-41-2394', 'BU1111');

INSERT INTO author_title (au_id, title_id)
	VALUES ('267-41-2394', 'TC7777'); PS: MariaDB [blog_db]> INSERT INTO author_title (au_id, title_id)
    ->  VALUES ('267-41-2394', 'TC7777');
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`blog_db`.`author_title`, CONSTRAINT `fk_title_title_id` FOREIGN KEY (`title_id`) REFERENCES `title` (`title_id`) ON UPDATE CASCADE)

INSERT INTO author_title (au_id, title_id)
	VALUES ('274-80-9391', 'BU7832');

INSERT INTO author_title (au_id, title_id)
	VALUES ('409-56-7008', 'BU1032');

INSERT INTO author_title (au_id, title_id)
	VALUES ('427-17-2319', 'PC8888');

INSERT INTO author_title (au_id, title_id)
	VALUES ('472-27-2349', 'TC7777'); PS: MariaDB [blog_db]> INSERT INTO author_title (au_id, title_id)
    ->  VALUES ('472-27-2349', 'TC7777');
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`blog_db`.`author_title`, CONSTRAINT `fk_title_title_id` FOREIGN KEY (`title_id`) REFERENCES `title` (`title_id`) ON UPDATE CASCADE)



2.) Given the following data, provide the answer to the following:

a.) List the book Authored by Marjorie Green = SELECT title from title WHERE EXISTS (SELECT au_id from author_title WHERE author_title.title_id = title.title_id AND au_id = '213-46-8915'); 

b.) List the book Authored by Michael O'Leary = SELECT title from title WHERE EXISTS (SELECT au_id from author_title WHERE author_title.title_id = title.title_id AND au_id = '267-41-2394');

c.) Write the author/s of "The Busy Executives Database Guide". = SELECT au_fname, au_lname from author WHERE EXISTS (SELECT title_id from author_title WHERE author_title.au_id = author.au_id AND title_id = 'BU1032');

d.) Identify the publisher of "But Is it User Friendly?" = SELECT pub_name from publisher WHERE EXISTS (SELECT title from title WHERE title.pub_id = publisher.pub_id AND title = 'But Is it User Friendly?'); 

e.) List the book published by Algodata Inforsystems. ( SELECT title.title FROM title where pub_id = 1389; )
